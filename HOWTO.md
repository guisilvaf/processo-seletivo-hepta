1. Instalação do GIT na máquina para clone do projeto.
2. Configurar Tomcat 9 (https://tomcat.apache.org/download-90.cgi).
3. Configurar o Jdk 8 (https://www.oracle.com/br/java/technologies/javase/javase8-archive-downloads.html).
4. Instalação de alguma IDE para execução do código.

Para testar aplicação acessar o serviço:
http://localhost:8080/funcionarios/rest/funcionarios/teste
http://localhost:8080/funcionarios/index.html
http://localhost:8080/funcionarios/pages/novo-funcionario.html