var cadastro = new Vue({
	el:"#cadastro",
    data: {
		listaSetor:[],

    },
    created: function(){
        let vm = this;
        vm.listarSetores();
    },
    methods:{
		listarSetores: function(){
			const vm = this;
			axios.get("/funcionarios/rest/setor")
			.then(response => {vm.listaSetor = response.data;
            }).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar os itens");
			}).finally(function() {
			});
		},
		listarFuncionarios: function(){
			const vm = this;
			axios.get("/funcionarios/rest/funcionarios")
			.then(response => {vm.lista = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar os itens");
			}).finally(function() {
			});
		},
		mostraAlertaErro: function(erro, mensagem){
			console.log(erro);
			alert(mensagem);
		}
    }
});
