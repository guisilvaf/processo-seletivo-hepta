var inicio = new Vue({
	el:"#inicio",
    data: {
        lista: []
    },
    created: function(){
        let vm =  this;
        vm.listarFuncionarios();
    },
    methods:{
	//Busca os itens para a lista da primeira página
        listarFuncionarios: function(){
			const vm = this;
			axios.get("/funcionarios/rest/funcionarios/listar")
			.then(response => {vm.lista = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar os itens");
			}).finally(function() {
			});
		},
		deletarFuncionarios: function(){
			const vm = this;
			axios.delete("/funcionarios/rest/funcionarios/deletar/{id}")
			.then(function (sucesso) {
				vm.mostraAlertaSucesso("Usuário excluído com sucesso")
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível excluir os itens");
			}).finally(function() {
			});
		},
		atualizarFuncionarios: function(){
			const vm = this;
			axios.get("/funcionarios/rest/funcionarios/atualizar")
			.then(function (sucesso) {
				vm.mostraAlertaSucesso("Usuário atualizado com sucesso")
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar os itens");
			}).finally(function() {
			});
		},
		mostraAlertaErro: function(erro, mensagem){
			console.log(erro);
			alert(mensagem);
		},
		mostraAlertaSucesso: function(sucesso, mensagem){
			console.log(sucesso);
			alert(mensagem);
		}
    }
});