package com.hepta.funcionarios.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.SetorDAO;

@Path("/setor")
public class SetorService {
    
    private SetorDAO dao;

    public SetorService(){
        dao = new SetorDAO();
    }

    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public Response setorCreate(Setor setor) {
        try {
            dao.save(setor);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.status(Status.OK).build();
    }

    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response setorRead() {
        List<Setor> setor = new ArrayList<>();
        try {
            setor = dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar setores").build();
        }

        GenericEntity<List<Setor>> entity = new GenericEntity<List<Setor>>(setor) {
        };
        return Response.status(Status.OK).entity(entity).build();
    }

    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PUT
    public Response setorUpdate(Setor setor) {
        return Response.status(Status.NOT_IMPLEMENTED).build();
    }

    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @DELETE
    public Response SetorDelete(@PathParam("id") Integer id) {
        return Response.status(Status.NOT_IMPLEMENTED).build();
    }

    @Path("/teste")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String testeJersey() {
        return "Funcionando.";
    }
}
